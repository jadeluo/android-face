package com.wis.face;

/**
 * Created by wis on 16-7-26.
 */
public class WisMobile {
    public  native void setNumThreads(int numThreads);

    public native void enableLog(boolean enabled);  // currently nonfunctional

    public native int loadModel(String modelDir);  // required

    //face
    public native float compare2Image(String imgFile1, String imgFile2);

    //face detect
    public native int[] detectFace(byte[] rgb32, int width, int height, int widthStep);

    //extract face feature
    public native float[] extractFeature(byte[] rgb32, int width, int height, int widthStep, int[] faceRect);

    //calculate two face feature similarity
    public native float compare2Feature(float[] fea1, float[] fea2);
}
